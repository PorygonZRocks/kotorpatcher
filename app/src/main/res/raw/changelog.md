[$PROFILE$]: extended

b1.1.0 {146}

    • Theme UI  
    • New launcher icon  
    • Changed minimum API level from 14 to 7  

    • *Switch from MarkdownView to txtmark*  
    • *Moved files from assets to raw resources*  
    • *No longer uses Xdroid.Toaster*  
    • *Now uses AppCompat support library and VectorDrawables*  
    • *Switch from official aFileChooser to Zhuowei Zang's version*  

b1.0.0 {13}

    • **REQUIRES REINSTALL - Changed signing key (I'm sorry, I forgot the old one)**  

    • **Not doing debug builds any more**  
   
    + 2DA support (thanks a ton @peedeeboy!)  
    + Option to create a 2DA from scratch (test)  
    + Do not install music or launcher (not supported by Android KOTOR)  

    • Log now updates while in progress  
    • App no longer freezes when installing mods  



a1.1.0 {2} (REQUIRES REINSTALL)

    • **REQUIRES REINSTALL - Changed app signature from debug key (not applicable to debug builds)**  

    • **Changed versioning from [Major].[minor] to [{a(lpha), b(eta), or r(release)}][Major].[minor].[patch]-[BUILD{debug only}]**  
   
    + File chooser  
    + Updater  

    • More detailed license info  
    • Licenses button moved to menu  
    • Log now has timestamps (in GMT)  
    • Minor (very minor) UI improvement  
   
   
   

a1.0.0 {1} (released as 1.0, see update a1.1.0)

    • **Initial release**
	
	
	
	
Format: 

Version {Version Code} (notes, if applicable)

    • **Really massively important stuff**

    + New features

    - Removed features

    • Changes

    • *Behind the scenes changes*
