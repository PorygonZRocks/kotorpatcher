/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg2da.editor;

import java.io.*;
import java.nio.*;
import java.util.*;
//import javax.swing.JList;

/**
 *
 * @author pete
 */

public class TwoDA {

    private String header;
    private List<String> columnNames;
    private List<String> rowNames;
    private int rowCount;
    private int columnCount;
    private Object[][] data;

    public TwoDA(String headerIn, List<String> columnNamesIn, 
				 List<String> rowNamesIn, int rowCountIn, int columnCountIn,
				 Object[][] dataIn) {

        this.header = headerIn;
        this.columnNames = columnNamesIn;
        this.rowNames = rowNamesIn;
        this.rowCount = rowCountIn;
        this.columnCount = columnCountIn;
        this.data = dataIn;

    }

    public String getHeader() {

        return this.header;

    }

    public List getColumnNames() {

        return this.columnNames;

    }

    public List getRowNames() {

        return this.rowNames;

    }

    public int getRowCount() {

        return this.rowCount;

    }

    public int getColumnCount() {

        return this.columnCount;
    }

    public Object[][] getData() {

        return this.data;

    }
}
