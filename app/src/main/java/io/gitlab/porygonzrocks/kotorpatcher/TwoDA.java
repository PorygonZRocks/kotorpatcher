package io.gitlab.porygonzrocks.kotorpatcher;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;

import java.io.IOException;
import java.io.FileNotFoundException;

import static java.nio.ByteOrder.LITTLE_ENDIAN;
import static java.nio.charset.StandardCharsets.US_ASCII;

public class TwoDA extends pkg2da.editor.TwoDA {

	private String header;
    private List<String> columnNames;
    private List<String> rowNames;
    private int rowCount;
    private int columnCount;
	private String[][] data;
    private List<List<String>> dataList;
	private File location;

	public TwoDA(pkg2da.editor.TwoDA input, File location) {
		super(input.getHeader(), 
			  input.getColumnNames(), 
			  input.getRowNames(), 
			  input.getRowCount(),
			  input.getColumnCount(),
			  input.getData());
		this.header = super.getHeader();
        this.columnNames = super.getColumnNames();
        this.rowNames = super.getRowNames();
        this.rowCount = super.getRowCount();
        this.columnCount = super.getColumnCount();
        this.dataList = new ArrayList<List<String>>(super.getData().length);                            // 2da data
		for (int i = 0; i < super.getData().length; i ++) {
			this.dataList.add(new ArrayList<String>(super.getData()[i].length));
			for (int j = 0; j < super.getData()[i].length; j++) {
				if (super.getData()[i][j].equals("")) {
					dataList.get(i).add("****");
				}
				else {
					dataList.get(i).add((String) super.getData()[i][j]);
				}
			}
		}
		syncData();
		this.location = location;
    }
	
	public void syncData() {															// Syncs array with list
		this.data = new String[this.rowCount][this.columnCount];
		for (int i = 0; i < rowCount; i++) {
			this.data[i] = new String[columnCount];
			for (int j = 0; j < columnCount; j++) {
				this.data[i][j] = this.dataList.get(i).get(j);
			}
		}
	}
	
	@Override
	public String[][] getData() {
		this.syncData();
		return this.data;
	}
	
	public File getFile() {
		return this.location;
	}
	
	public List<List<String>> getDataList() {
		return this.dataList;
	}
	
	public void setData(int row, int column, String contents) {
		this.dataList.get(row).set(column, contents);
	}

	public void addRow(String row) {
		this.dataList.add(new ArrayList<String>(this.columnCount));
		for (int i = 0; i < this.columnCount; i++) {
			this.dataList.get(this.rowCount).add("****");
		}
		this.rowCount++;
		this.rowNames.add(row);
	}

	public void addColumn(String column) {
		for (int i = 0; i < this.rowCount; i++) {
			this.dataList.get(i).add("****");
		}
		this.columnCount++;
		this.columnNames.add(column);
	}

	// write() is by peedeeboy
	public void write() {

        /* Variables */
        // Constants
        final byte[] FILE_HEADER = this.header.getBytes();             // "2DA V2.b" followed by LF
        final byte NUL = 0x00;                                                  // NULL byte
        final byte TAB = 0x09;                                                  // TAB byte
		final byte LF  = 0x0A;													// LINE FEED byte

		try {
			/* Do the work! */
			try(FileOutputStream fos = new FileOutputStream(this.location)) {                // "Try with resources" (Stream closes automatically)

				// Write header
				fos.write(FILE_HEADER);                                             // Write header
				fos.write(LF);														// Write line fees

				// Write column headers     
				for (String header : this.columnNames) {                                   // Loop through column headers
					fos.write(header.getBytes(US_ASCII));                      // Convert String ASCII bytes and write to file
					fos.write(TAB);                                                 // Write TAB delimiter
				}

				// Write NULL delimiter to end of column headers section
				fos.write(NUL);

				// Write number of rows (32 bit unigned integer)
				ByteBuffer rowBuffer = ByteBuffer.allocate(4)                       // Create a byte rowBuffer of 4 bytes
					.order(LITTLE_ENDIAN)              // Little Endian byte order
					.putInt(this.rowNames.size());        // containing the number of rows
				fos.write(rowBuffer.array());                                       // Write number of rows to file
				rowBuffer.clear();                                                  // Empty the byte rowBuffer

				// Write row headers
				for (String header : this.rowNames) {                                   // Loop through row headers            
					fos.write(header.getBytes(US_ASCII));                      // Convert String ASCII bytes and write to file
					fos.write(TAB);                                                 // Write TAB delimiter
				}

				// Write offsets
				short offset = 0;                                                   // Keep a running total of how far into data we are
				Map<String, Short> map = new LinkedHashMap<>();                     // Keep an ordered map of Data/Offset

				ByteBuffer offsetBuffer = ByteBuffer.allocate(2)                    // Create a byte rowBuffer of 2 bytes
					.order(LITTLE_ENDIAN);           		  // Little Endian byte order

				for (List<String> row : dataList) {                                      // Loop through rows
					for (String element : row) {                                     // loop through each cell in row

					if (element.equals("****")) {									// Replace **** with blank string
						element = "";
					} 
					
						offsetBuffer.clear();                                       // Empty buffer

						if (map.containsKey(element)) {                              // If data has been encountered before
							offsetBuffer.putShort(map.get(element));                // get the existing offset for it
						}
						else {                                                      // otherwise,
							offsetBuffer.putShort(offset);                          // use the current running offset
							map.put(element, offset);                               // store the data and offset in the map
							if (element.length() > 0) {                              // If data is not an empty string
								offset += element.length();                         // update the offset for the next unique piece of data
							}                            // 
							offset++;                                               // add +1 to offset to account for NULL byte delimiter
						}

						fos.write(offsetBuffer.array());                            // Write offset to file
					}
				}

				// Write two NULL bytes to mark end of offsets and start of data    
				fos.write(new byte[]{NUL, NUL});                                    // Write two null bytes

				// Write data
				for (String key : map.keySet()) {
					if (key.length() == 0) {                                         // If data is an empty string
						fos.write(NUL);                                             // write a single NULL byte
					}
					else {                                                          // if data is not empty,
						fos.write(key.getBytes(US_ASCII));                     // convert String ASCII bytes and write to file
						fos.write(NUL);                                             // Write NUL delimiter
					}

				}

			}
		}
		catch (IOException e) {                                              // Something went wrong
            Log.e("TwoDA", "Could not write file!", e);                                               // so print stack trace
        }
    }
}
