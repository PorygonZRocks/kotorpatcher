package io.gitlab.porygonzrocks.kotorpatcher;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import com.github.rjeschke.txtmark.Processor;
import java.io.File;

import java.io.IOException;

public class MarkdownViewer extends AppCompatActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.markdown);
		TextView mdView = (TextView) findViewById(R.id.markdownView);
		mdView.setMovementMethod(LinkMovementMethod.getInstance());
		String data = getIntent().getStringExtra("io.gitlab.porygonzrocks.kotorpatcher.Data");
		String htmlCode = null;
		if (getIntent().getBooleanExtra("io.gitlab.porygonzrocks.kotorpatcher.IsPath", false)) {
			File mdFile = new File(data);
			try {
				htmlCode = Processor.process(mdFile);
			}
			catch (IOException e) {}
		}
		else {
			htmlCode = Processor.process(data);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			mdView.setText(Html.fromHtml(htmlCode, Html.FROM_HTML_MODE_LEGACY));
		}
		else {
			mdView.setText(Html.fromHtml(htmlCode));
		}
	}
	
}
